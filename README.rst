.. -*- coding: utf-8 -*-
.. :Project:   split_repo
.. :Created:   dom 09 dic 2018 12:29:11 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 Lele Gaifax
..

============
 split_repo
============

Split a big git repository into smaller ones
============================================

:version: 1.0
:author: Lele Gaifax <lele@metapensiero.it>
:license: GPLv3

Recently I had to work on a very old set of projects versioned with M$ Source Safe, made by
illiterate programmers who knew nothing about good habits...

I exported the whole VSS storage into a big Git repository using vss2git__, but the result was
still a mess, full of artifacts, without the palest idea of a naming convention for the top
level projects.

__ https://github.com/trevorr/vss2git

First of all I tried with a script that interated over all of them, executing a ``git subtree
split`` to extract each top level directory into a separate repository, but I quickly hit show
stopper issues with its handling of `prefixes` containing whitespaces. It also was awfully
slow, because each split have to examine the whole range of commits...

So I decided to write my own custom solution. First I tried pygit2__, just to find it severely
under-documented and hard to use. Then I switched to dulwich__, an excellent even if slower
alternative.

__ https://www.pygit2.org/
__ https://www.dulwich.io/

The resulting script crawls over all the repository's history, splitting out each commit into
several different repositories, one for each top level directory possibly renaming it with some
grain of salt. It also ignores several artifacts such as compiled stuff, or the classic
``xxx.OLD`` files, commonly created by those who don't trust their own version control tool.
